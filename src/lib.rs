extern crate goblin;
extern crate hex;

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::Read;
use std::path::Path;

pub mod error;

use error::Error;
use error::Result;

/// A Chunk of memory, with its absolute address, as well as its length
#[derive(Debug)]
pub struct AdeptData {
    address: u64,
    length: usize,
    data: Vec<u8>,
}

impl AdeptData {
    /// Get data base address
    pub fn get_base_address(&self) -> u64 {
        self.address
    }

    /// Get data length
    pub fn get_contents_length(&self) -> usize {
        self.length
    }

    /// Get all contents, consuming the structure
    pub fn get_contents(self) -> Vec<u8> {
        self.data
    }

    /// Get a byte by address
    ///
    /// # Inputs
    ///
    /// The function an address for the byte to be read
    pub fn get_byte(&self, address: usize) -> Result<u8> {
        if address > self.length {
            return Err(Error::AdeptBoundsError);
        }
        Ok(self.data[address])
    }

    /// Get a word by address, aligned to wordsize
    ///
    /// # Inputs
    ///
    /// The function an address for the word to be read
    pub fn get_word(&self, address: usize) -> Result<u32> {
        let masked = (address >> 2) << 2;

        if masked > self.length {
            return Err(Error::AdeptBoundsError);
        }

        let bytes = &(self.data[masked..masked + 4]);

        Ok(u32::from(bytes[0])
            + (u32::from(bytes[1]) << 8)
            + (u32::from(bytes[2]) << 16)
            + (u32::from(bytes[3]) << 24))
    }
}

/// Public function for main program not having to call 2 different functions
/// to get the data from file
///
/// # Inputs
///
/// The file path and the flag value if the file is .hex
///
/// # Outputs
///
/// The function returns a Vec of memory Chunks
pub fn get_adept_data(file_path: &str, file_is_hex: bool) -> Result<Vec<AdeptData>> {
    if file_is_hex {
        get_adept_data_from_hex(file_path)
    } else {
        get_adept_data_from_elf(file_path)
    }
}

/// Returns the contents of an elf file
///
/// # Inputs
///
/// The function requires a file path for the elf binary
///
/// # Outputs
///
/// The function returns a Vec of memory Chunks
fn get_adept_data_from_elf(elf_path: &str) -> Result<Vec<AdeptData>> {
    // Open and read file
    let path = Path::new(&elf_path);
    let mut fd = File::open(path)?;
    let mut buffer = Vec::new();

    fd.read_to_end(&mut buffer)?;

    // Parse file as elf
    let elf = goblin::elf::Elf::parse(&buffer)?;

    // Get program data from elf
    let size = elf.program_headers.len();

    let mut result = Vec::with_capacity(size);

    for program_header in &elf.program_headers {
        let file_offset = program_header.p_offset as usize;
        let address = program_header.p_paddr as u64;
        let length = program_header.p_filesz as usize;

        if length == 0 {
            continue;
        }

        let mut data = Vec::with_capacity(length);

        for counter in 0..length {
            data.push(buffer[file_offset + counter]);
        }

        result.push(AdeptData {
            address,
            length,
            data,
        });
    }

    result.shrink_to_fit();

    Ok(result)
}

/// Returns the contents of an hex file
///
/// # Inputs
///
/// The function requires a file path for the hex file
///
/// # Outputs
///
/// The function returns a Vec of memory Chunks
fn get_adept_data_from_hex(hex_path: &str) -> Result<Vec<AdeptData>> {
    // Open and read file
    let path = Path::new(&hex_path);
    let fd = File::open(path)?;
    let mut reader = BufReader::new(fd);

    let mut address: u64 = 0; // Start from address 0
    let length: usize = 4; // Each instruction has 4 bytes

    let mut result = Vec::new();

    loop {
        let mut line = String::new();

        let len = reader.read_line(&mut line)?; // Read the line

        // Get out of loop when EOF is reached
        if line.is_empty() {
            break;
        }

        // The .hex files come with 10 chars in each line: the 8 numbers,
        // the \n at the end and the " " at the start
        if len != 10 {
            break;
        }

        // Take out \n at the end and the " " at the start
        line.pop();
        line.remove(0);

        let mut data = hex::decode(line).unwrap(); // using the hex crate...

        // ... but the data comes in a different order than the one used
        // in this program, so 1st and last byte are changed and 2nd and 3rd too
        data.swap(0, 3);
        data.swap(1, 2);

        // Then push the line to AdeptData struct
        result.push(AdeptData {
            address,
            length,
            data,
        });

        address += 4 as u64; // Each instruction has 4 bytes
    }

    result.shrink_to_fit();

    Ok(result)
}
